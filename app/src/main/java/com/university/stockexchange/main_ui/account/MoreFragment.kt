package com.university.stockexchange.main_ui.account

import android.app.UiModeManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.switchmaterial.SwitchMaterial
import com.university.stockexchange.MainActivity
import com.university.stockexchange.R
import com.university.stockexchange.ServiceApp
import com.university.stockexchange.login.LoginActivity
import kotlinx.android.synthetic.main.fragment_more.*

class MoreFragment : Fragment() {

  private lateinit var moreViewModel: MoreViewModel

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    moreViewModel = ViewModelProvider(this).get(MoreViewModel::class.java)
    val rootView = inflater.inflate(R.layout.fragment_more, container, false)
    val prefs = context?.getSharedPreferences("MyPref", AppCompatActivity.MODE_PRIVATE)

    moreViewModel.fetchQuestList((activity?.application as? ServiceApp)?.serviceApi, prefs?.getString("api_token", "-1")!!)
    moreViewModel.getAccountData()?.observe(viewLifecycleOwner, {
      it?.let {
        tvEmail.text = it.email
      }
    })
    val exitButton = rootView.findViewById<Button>(R.id.login_button)
    val switcher = rootView.findViewById<SwitchMaterial>(R.id.change_mode_switch)
    exitButton.setOnClickListener { logout() }
    switcher.setOnCheckedChangeListener { buttonView, isChecked ->  changeMode(buttonView, isChecked)}
    return rootView
  }

  private  fun changeMode(view: View, isChecked: Boolean): Boolean {
    when (isChecked) {
      true -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)

      false -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
    return true
  }

  private fun logout() {
      val edit = context?.getSharedPreferences("MyPref", AppCompatActivity.MODE_PRIVATE)?.edit()
      edit?.putString("api_token", null)?.apply()
      val intent = Intent(context, LoginActivity::class.java)
      startActivity(intent)
      activity?.finish()

  }


}